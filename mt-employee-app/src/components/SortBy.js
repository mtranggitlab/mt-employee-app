import React from 'react'
import styled from '@emotion/styled'

const Select = styled.select`
  outline: 0;
  padding: 0.6rem;
  margin-left: 5px;
  border: 1px solid rgba(34, 36, 38, 0.15);
  border-radius: 3px;
  min-width: 150px;
  &:focus,
  &:active {
    border-color: #85b7d9;
  }
`

const SortBy = props => {
  return (
    <div>
      Sort by:
      <Select onChange={props.onChange} value={props.value}>
        <option value="firstName">first name</option>
        <option value="lastName">last name</option>
      </Select>
    </div>
  )
}

export default SortBy
