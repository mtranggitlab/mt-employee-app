import React from 'react'
import {Dialog} from '@reach/dialog'
import VisuallyHidden from '@reach/visually-hidden'
import '@reach/dialog/styles.css'
import {getEmployeeById} from '../api'
import Card from './Card'

const Modal = props => {
  const {cardId, close} = props
  const [employee, setEmployee] = React.useState(null)
  const [loading, setLoading] = React.useState(true)

  React.useEffect(() => {
    async function fetchEmployee() {
      setLoading(true)
      const employee = await getEmployeeById(cardId)
      setEmployee(employee)
      setLoading(false)
    }
    fetchEmployee()
  }, [cardId])

  return (
    props.showDialog && (
      <Dialog aria-label="Employee card details" onDismiss={close}>
        <button className="close-button" onClick={close}>
          <VisuallyHidden>Close</VisuallyHidden>
          <span aria-hidden>&times;</span>
        </button>
        <div>{loading ? 'Loading...' : <Card {...employee} full />}</div>
      </Dialog>
    )
  )
}

export default Modal
