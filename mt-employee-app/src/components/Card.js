import React from 'react'
import {format} from 'date-fns'
import styled from '@emotion/styled'

const EmployeeInfo = styled.div`
  display: grid;
  grid-gap: 0.6rem;
  grid-template-columns: 130px 1fr;
  align-items: start;
  padding: 1rem;
  border: 1px solid lightgrey;
  .fullName {
    font-weight: bold;
  }
  .bio {
    margin-top: 0.6rem;
  }
`

const EmployeeFullInfo = styled.div`
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: 130px 1fr;
  grid-template-rows: 130px 1fr;
  align-items: start;
  justify-items: start;
  padding: 1rem;
  .jobTitle {
    font-weight: bold;
  }
  .fullName {
    border-bottom: 1px solid lightgrey;
    font-weight: bold;
    font-size: 1.5rem;
    align-self: end;
    padding: 1rem 0;
  }
`

const Card = props => {
  const {
    firstName,
    lastName,
    jobTitle,
    age,
    dateJoined,
    avatar,
    bio,
    full = false,
  } = props
  const fullName = `${firstName} ${lastName}`
  if (full === false) {
    return (
      <EmployeeInfo>
        <img alt={fullName} src={avatar} />
        <div className="details">
          <div className="fullName">{fullName}</div>
          <div className="bio">{bio}</div>
        </div>
      </EmployeeInfo>
    )
  }
  return (
    <EmployeeFullInfo>
      <img alt={fullName} src={avatar} />
      <div className="fullName">{fullName}</div>
      <div className="details">
        <div className="jobTitle">{jobTitle}</div>
        <div className="age">{age}</div>
        <div className="dateJoined">
          {dateJoined && format(new Date(dateJoined), 'dd-MMM-yyyy')}
        </div>
      </div>
      <div className="bio">{bio}</div>
    </EmployeeFullInfo>
  )
}

export default Card
