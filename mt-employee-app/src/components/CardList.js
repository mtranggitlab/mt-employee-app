import React from 'react'
import Card from './Card'
import Modal from './Modal'
import styled from '@emotion/styled'

const StyledSection = styled.section`
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  .card {
    cursor: pointer;
  }
`

const CardList = props => {
  const [showDialog, setShowDialog] = React.useState(false)
  const open = () => setShowDialog(true)
  const close = () => setShowDialog(false)
  const [cardId, setCardId] = React.useState(null)

  const showCard = cardId => {
    setCardId(cardId)
    open()
  }

  if (props.error) {
    return <p>{props.error}</p>
  }

  if (props.loading) {
    return <p>Loading...</p>
  }

  if (props.searching) {
    return <p>Searching...</p>
  }

  if (props.cards.length === 0) {
    return <p>No results.</p>
  }

  return (
    <React.Fragment>
      <StyledSection>
        {props.cards.map((card, index) => (
          <div
            key={`${card.id}`}
            onClick={() => showCard(card.id)}
            className="card"
          >
            <Card {...card} />
          </div>
        ))}
      </StyledSection>
      <Modal showDialog={showDialog} close={close} cardId={cardId} />
    </React.Fragment>
  )
}

export default CardList
