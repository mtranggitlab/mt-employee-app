import React from 'react'
import {getCompanyInfo} from '../api'
import {format} from 'date-fns'
import styled from '@emotion/styled'

const Logo = styled.h1`
  font-size: 3rem;
  position: relative;
  transform: skew(-7deg);
  a {
    padding: 0.5rem 1rem;
    background: teal;
    color: #fafafa;
    text-transform: uppercase;
    text-decoration: none;
    @media (max-width: 800px) {
      line-height: 4rem;
    }
  }
`

const Motto = styled.div`
  font-weight: bold;
  text-transform: capitalize;
  transform: skew(-7deg);
`

const EstablishedSince = styled.div`
  font-weight: bold;
  transform: skew(-7deg);
`
const StyledHeader = styled.header`
  background-color: #e0f2f1;
  .bar {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: flex-end;
    border-bottom: 5px solid var(--black);
    padding: 1rem;
    @media (max-width: 800px) {
      flex-direction: column;
      align-items: flex-start;
    }
  }
`

const Header = props => {
  const [company, setCompany] = React.useState(null)
  const [loading, setLoading] = React.useState(false)
  const [error, setError] = React.useState('')

  React.useEffect(() => {
    async function fetchData() {
      try {
        setLoading(true)
        const company = await getCompanyInfo()
        console.log(company)
        setCompany(company)
        setLoading(false)
        setError('')
      } catch (e) {
        console.warn('Error on fetching data', e)
        setError('Error of fetching company info. Try again.')
        setLoading(false)
      }
    }
    fetchData()
  }, [])
  if (loading || company === null) {
    return <p>Loading...</p>
  }
  if (error) {
    return <p>{error}</p>
  }

  return (
    <StyledHeader>
      <div className="bar">
        <div>
          <Logo>
            <a href="/">{company.companyName}</a>
          </Logo>
          <Motto>{company.companyMotto}</Motto>
        </div>
        <EstablishedSince>
          Since {`${format(new Date(company.companyEst), 'yyyy')}`}
        </EstablishedSince>
      </div>
    </StyledHeader>
  )
}

export default Header
