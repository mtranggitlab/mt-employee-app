import React, {useState, useEffect} from 'react'
import {getEmployees} from '../api'
import Search from './Search'
import SortBy from './SortBy'
import CardList from './CardList'
import useDebounce from '../utils/useDebounce'
import styled from '@emotion/styled'

const Container = styled.div`
  padding: 1rem;
  border: 1px solid lightgrey;
  .subbar {
    display: grid;
    grid-template-columns: auto 210px 262px;
    grid-gap: 1rem;
    align-items: baseline;
    border-bottom: 2px solid var(--black);
    margin-bottom: 1rem;
    @media (max-width: 800px) {
      grid-template-columns: 1fr;
      justify-content: center;
      padding-bottom: 1rem;
    }
  }
`

function EmployeesGrid() {
  const [employees, setEmployees] = useState([])
  const [loading, setLoading] = useState(true)
  const [searching, setSearching] = useState(false)
  const [error, setError] = React.useState('')

  const [searchTerm, setSearchTerm] = useState('')
  const [sortBy, setSortBy] = useState('firstName')
  const handleSearchTermChange = e => setSearchTerm(e.target.value)
  const handleSortByChange = e => setSortBy(e.target.value)
  const debouncedSearchTerm = useDebounce(searchTerm, 500)

  useEffect(() => {
    async function fetchData() {
      try {
        if (debouncedSearchTerm) {
          setSearching(true)
          setLoading(false)
        } else {
          setSearching(false)
          setLoading(true)
        }
        const employees = await getEmployees(debouncedSearchTerm)
        if (sortBy) {
          employees.sort((a, b) => (a[sortBy] > b[sortBy] ? 1 : -1))
        }
        setEmployees(employees)
        setSearching(false)
        setLoading(false)
      } catch (e) {
        console.warn('Error on fetching data', e)
        setError('Error of fetching employees data. Please try again.')
        setSearching(false)
        setLoading(false)
      }
    }
    fetchData()
  }, [sortBy, debouncedSearchTerm])

  return (
    <Container>
      <div className="subbar">
        <h3>Our employees</h3>
        <SortBy onChange={handleSortByChange} value={sortBy} />
        <Search onChange={handleSearchTermChange} value={searchTerm} />
      </div>
      {
        <CardList
          cards={employees}
          loading={loading}
          searching={searching}
          error={error}
        />
      }
    </Container>
  )
}

export default EmployeesGrid
