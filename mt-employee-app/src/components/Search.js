import React from 'react'
import styled from '@emotion/styled'

const Input = styled.input`
  outline: 0;
  padding: 0.6rem;
  margin-left: 5px;
  border: 1px solid rgba(34, 36, 38, 0.15);
  border-radius: 3px;
  min-width: 150px;
  &:focus,
  &:active {
    border-color: #85b7d9;
  }
`

const Search = props => {
  return (
    <div>
      Search:{' '}
      <Input
        type="search"
        placeholder="Search employees"
        value={props.value}
        onChange={props.onChange}
      />
    </div>
  )
}

export default Search
