import employeeData from './data/sample-data.json'

// HELPERS
const searchEmployees = search => {
  const {employees} = employeeData
  const filteredEmployees = employees.filter(
    employee =>
      employee.firstName.toLowerCase().indexOf(search) > -1 ||
      employee.lastName.toLowerCase().indexOf(search) > -1,
  )
  return filteredEmployees
}

export function getEmployees(searchTerm) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // reject('Oops, error occurs')
      let {employees: employeeList} = employeeData
      const search = searchTerm ? searchTerm.trim().toLowerCase() : ''
      if (search) {
        employeeList = searchEmployees(search)
      }
      const mappedEmployees = employeeList.map(
        ({id, firstName, lastName, bio, avatar}) => ({
          id,
          firstName,
          lastName,
          avatar,
          bio: `${bio.slice(0, 20)}...`,
        }),
      )
      return resolve(mappedEmployees)
    }, 1000)
  })
}

export function getEmployeeById(id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const {employees} = employeeData
      const employee = employees.find(e => e.id === id)
      return resolve(employee)
    }, 200)
  })
}

export function getCompanyInfo() {
  const {companyInfo} = employeeData
  return Promise.resolve(companyInfo)
}
