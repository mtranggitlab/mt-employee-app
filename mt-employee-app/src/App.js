import React from 'react'
import EmployeesGrid from './components/EmployeesGrid'
import Header from './components/Header'

// Adding global css
import {Global, css} from '@emotion/core'
import normalize from 'normalize.css'

function App() {
  return (
    <React.Fragment>
      <Global
        styles={css`
          ${normalize}
          body {
            background-color: #fafafa;
            margin: 0 auto;
            padding: 0 1rem !important;
          }
        `}
      />
      <Header />
      <EmployeesGrid />
    </React.Fragment>
  )
}

export default App
